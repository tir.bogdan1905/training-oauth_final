const Sequelize = require("sequelize");

const sequelize = new Sequelize("test", "root", "", {
  dialect: "mysql",
  host: "localhost",
  define: {
    timestamps: true,
  },
});

module.exports = sequelize;
