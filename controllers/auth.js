const passport = require("passport");
const GoogleStrategy = require("passport-google-oauth20").Strategy;
const User = require("../models").User;

passport.serializeUser((user, done) => {
  //done(null, user.id);
  done(null, 1);
});

passport.deserializeUser(async (id, done) => {
  //const user = await User.findOne({ where: { id } });
  //done(null, user);
  done(null, 1);
});

passport.use(
  new GoogleStrategy(
    {
      clientID: process.env.CLIENT_ID,
      clientSecret: process.env.CLIENT_SECRET,
      callbackURL: "http://localhost:8081/api/auth/google/redirect",
    },
    (accessToken, refreshToken, email, done) => {
      User.findOne({
        where: {
          email: email.emails[0].value,
        },
      }).then((currentUser) => {
        if (currentUser == null) {
          User.create({
            firstName: email.name.givenName,
            lastName: email.name.familyName,
            email: email.emails[0].value,
          })
            .then((user) => {
              done(null, user);
            })
            .catch((err) => {
              console.log(err);
            });
        } else {
          done(null, currentUser);
        }
      });
    }
  )
);
